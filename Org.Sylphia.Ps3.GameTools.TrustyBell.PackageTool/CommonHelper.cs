﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace Org.Sylphia.Ps3.GameTools.TrustyBell.PackageTool
{
    internal static class CommonHelper
    {
        public static int PaddingInt(int x,int paddingSize)
        {
            return ((x + paddingSize)/paddingSize)*paddingSize;
        }

        public static string ReadLPZStr(BinaryReader rdr,int maxLength,Encoding encoding)
        {
            var buffer = new byte[maxLength];
            for (var i = 0; i < maxLength; i++)
            {
                buffer[i] = rdr.ReadByte();
                if( buffer[i] != 0 ) continue;
                return i==0?"":encoding.GetString(buffer, 0, i);
            }
            return encoding.GetString(buffer, 0, maxLength);
        }

        public static string FormatBytes(long bytes)
        {
            const int scale = 1024;
            var orders = new[] { "GB", "MB", "KB", "字节" };
            var max = (long)Math.Pow(scale, orders.Length - 1);

            foreach (var order in orders)
            {
                if (bytes > max)
                    return string.Format("{0:##.##} {1}", decimal.Divide(bytes, max), order);

                max /= scale;
            }
            return "0 字节";
        }

        public static int HtoN(int x)
        {
            return IPAddress.HostToNetworkOrder(x);
        }

        public static long HtoN(long x)
        {
            return IPAddress.HostToNetworkOrder(x);
        }

        public static int NtoH(int x)
        {
            return IPAddress.NetworkToHostOrder(x);
        }

        public static uint NtoH(uint x)
        {
            return (uint)IPAddress.NetworkToHostOrder((int)x);
        }
    }
}
