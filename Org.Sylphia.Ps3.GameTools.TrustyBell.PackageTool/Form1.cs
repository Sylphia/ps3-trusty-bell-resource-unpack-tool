﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Org.Sylphia.Ps3.GameTools.TrustyBell.PackageTool.PackageHandler;

namespace Org.Sylphia.Ps3.GameTools.TrustyBell.PackageTool
{
    public partial class Form1 : Form
    {
        private KeyValuePair<Rectangle, byte[]> _data;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            int max = int.MaxValue;
            var s = "";
            foreach (string v in FntHandler.GlyphData.Keys)
            {
                var val = (KeyValuePair<Rectangle, byte[]>)FntHandler.GlyphData[v];
                if (val.Value.Length < max)
                {
                    max = val.Value.Length;
                    s = v;
                }
            }
            Console.WriteLine(max);
            Console.WriteLine(s);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!FntHandler.GlyphData.ContainsKey(textBox1.Text))
            {
                label2.Text = string.Format("No key {0}", textBox1.Text);
                return;
            }

            _data = (KeyValuePair<Rectangle, byte[]>)FntHandler.GlyphData[textBox1.Text];

            //var width = int.Parse(textBox2.Text);
            //var height = int.Parse(textBox3.Text);

            var width = _data.Key.Width + _data.Key.X;
            var height = _data.Key.Height + _data.Key.Y;

            var bmp = ImageHelper.DrawFont(_data.Key.X, _data.Key.Y, _data.Key.Width, _data.Key.Height, _data.Value);

           
            //var bmd = bmp.LockBits(new Rectangle(0, 0, width, height),
            //                       ImageLockMode.ReadWrite, bmp.PixelFormat);

            //Console.WriteLine("{0},{1},{2},{3}",_data.Key.X,_data.Key.Y,_data.Key.Width,_data.Key.Height);
            //Console.WriteLine(_data.Value.Length);

            ////if((_data.Value.Length)> (height * bmd.Stride + width>>3)) return;
            //var ptr = bmd.Scan0;
            //var index = _data.Key.X*bmd.Stride + _data.Key.Y >> 3;
            //for (var i = 0; i < _data.Value.Length; i++)
            //{
            //    Marshal.WriteByte(ptr,index+i, _data.Value[i]);
            //}

            //bmp.UnlockBits(bmd);

            pictureBox1.Image = bmp;
            
        }
    }
}
