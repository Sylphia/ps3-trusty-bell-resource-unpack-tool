﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Windows.Forms;
using Org.Sylphia.Ps3.GameTools.TrustyBell.PackageTool.FileFormats.E;
using Org.Sylphia.Ps3.GameTools.TrustyBell.PackageTool.PackageHandler;

namespace Org.Sylphia.Ps3.GameTools.TrustyBell.PackageTool
{
    static class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            //FntHandler.Parse(@"D:\Download\Games\PS3\信赖铃音\BLJS10017\PS3_GAME\USRDIR\archives\root\p1.fnt");
            //Application.Run(new Form1());

            //return;
            
            // Test for unpack strings
            var efile = new EFile(@"D:\Download\Games\PS3\信赖铃音\BLJS10017\PS3_GAME\USRDIR\archives\cfdata\dld18.e");

            foreach (var file in Directory.GetFiles(@"D:\Download\Games\PS3\信赖铃音\BLJS10017\PS3_GAME\USRDIR\archives\cfdata","*.e",SearchOption.TopDirectoryOnly))
            {
                EPackager.Unpack(file);
            }

            using(var fs= File.OpenWrite("strings.txt"))
            {
                using (var wtr = new StreamWriter(fs))
                {
                    foreach (var key in EPackager.Strings.Keys)
                    {
                        wtr.WriteLine(key);
                        wtr.WriteLine(EPackager.Strings[key]);
                        wtr.WriteLine();
                    }
                }
            }

            Console.ReadKey();
            return;

            // Unpack .files
            if (args.Length == 1)
            {
                {
                    var path = args[0];

                    if (!File.Exists(path) && !Directory.Exists(path))
                    {
                        Console.WriteLine("无效的输入路径{0}。", path);
                        return;
                    }

                    if (File.Exists(path))
                    {
                        FilesPackager.Unpack(path);
                    }
                    else
                    {
                        var di = new DirectoryInfo(path);
                        FilesPackager.Pack(di.FullName);
                    }
                }
            }
            else
            {
                Console.WriteLine(
                    "信赖铃音文件包工具 v1.0 By Sylphia.\nMail me @ admin@sylphia.org\n\n用法：{0} <路径名>\n1.解包: <路径名>是一个.files文件。 \n2.打包：<路径名>是一个文件夹。将一个目录及其子目录内文件打包到一个.files文件中，如果目录名为root，则只打包其中文件而不包含路径。\n注意：该工具会覆盖任何已存在的文件。",
                    Environment.GetCommandLineArgs()[0]);
                return;
            }
        }

    } 
}
