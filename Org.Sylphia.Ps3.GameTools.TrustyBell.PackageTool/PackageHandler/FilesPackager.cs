﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Org.Sylphia.Ps3.GameTools.TrustyBell.PackageTool.PackageHandler
{
    internal static class FilesPackager
    {
        private const int HeaderMagic = 0x454c4946;
        private const int Align = 0x800;

        static int GetHeaderSize(int fileCount)
        {
            var assumedHeaderSize = fileCount * 0x30 + 0x10;
            if (assumedHeaderSize % Align != 0)
            {
                assumedHeaderSize = (assumedHeaderSize / Align + 1) * Align;
            }
            return assumedHeaderSize;
        }


        public static void Pack(string path)
        {
            var ret = 0;

            var files = Directory.GetFiles(path, "*", SearchOption.AllDirectories);
            var totalFileSize = 0L;
            foreach (var file in files)
            {
                totalFileSize += new FileInfo(file).Length;
            }

            var size = 0u;
            var fileCount = files.Length;

            var fileOffset = (uint)GetHeaderSize(fileCount);

            var parent = new DirectoryInfo(path).Parent;
            Debug.Assert(parent != null);

            var dirName = path.Substring(path.LastIndexOf('\\') + 1);
            var packageName = dirName + ".files";

            var outFs = new FileStream(packageName, FileMode.Create);
            using (var wtr = new BinaryWriter(outFs))
            {
                wtr.Write(HeaderMagic);
                wtr.Write(CommonHelper.NtoH((int)size));
                wtr.Write(CommonHelper.NtoH(fileCount));
                wtr.Write(0);

                if (totalFileSize > 200 * 1048576)
                {
                    Console.Clear();
                    Console.WriteLine("正在打包{0}，总大小{1}。\n请耐心等待，该操作可能需要几分钟。", packageName,
                                      CommonHelper.FormatBytes(fileOffset + totalFileSize));
                }

                var processedSize = 0L;
                var lastPercent = 0;
                for (var i = 0; i < fileCount; i++)
                {
                    var fileNameBuffer = new byte[0x20];
                    var fileNameBytes = Encoding.UTF8.GetBytes(files[i].Replace(parent.FullName + "\\" + (dirName.ToLower() == "root" ? "root\\" : ""), "").ToLower());
                    if (fileNameBytes.Length > 0x20)
                    {
                        Console.WriteLine("检测到超长文件名{0}，无法继续。", files[i]);
                        ret = -1;
                        break;
                    }
                    fileNameBytes.CopyTo(fileNameBuffer, 0);

                    using (var fs = new FileStream(files[i], FileMode.Open))
                    {
                        wtr.Write(fileNameBuffer);
                        wtr.Write(CommonHelper.NtoH((int)fileOffset));
                        wtr.Write(CommonHelper.NtoH((int)fs.Length));

                        wtr.Write(0L);

                        var currentPos = outFs.Position;
                        outFs.Seek(fileOffset, SeekOrigin.Begin);

                        var bytesRemain = (int)fs.Length;
                        var bytesToRead = 0x800;
                        var bytesWrote = 0;
                        while (bytesRemain > 0)
                        {
                            var buf = new byte[0x800];
                            if (bytesRemain < 0x800) bytesToRead = bytesRemain;
                            var readbytes = fs.Read(buf, 0, bytesToRead);
                            Debug.Assert(readbytes == bytesToRead);
                            outFs.Write(buf, 0, 0x800);
                            bytesWrote += 0x800;
                            bytesRemain -= readbytes;

                            processedSize += readbytes;
                            Console.SetCursorPosition(0, 2);
                            Console.CursorVisible = false;
                            var percent = (int)Math.Round(processedSize / (double)totalFileSize * 100);
                            if (percent <= lastPercent) continue;
                            lastPercent = percent;
                            Console.WriteLine("已完成{0}%...", percent);
                        }
                        Console.CursorVisible = true;
                        fileOffset += (uint)bytesWrote;

                        outFs.Seek(currentPos, SeekOrigin.Begin);
                    }
                }

                outFs.Flush();
                size = (uint)outFs.Length;
                outFs.Seek(4, SeekOrigin.Begin);
                wtr.Write(CommonHelper.NtoH((int)size));
            }
            outFs.Dispose();
            if (ret == 0)
                Console.WriteLine("已成功创建{0}。", packageName);
            else
            {
                File.Delete(packageName);
                Console.WriteLine("{0}创建失败。", packageName);
            }
        }

        public static void Unpack(string fileName)
        {
            var fs = new FileStream(fileName, FileMode.Open);
            using (var rdr = new BinaryReader(fs))
            {
                var buf = new byte[0x400];
                var totalSize = 0L;
                var header = rdr.ReadInt32();
                if (header == HeaderMagic)
                {
                    var size = (uint)CommonHelper.HtoN(rdr.ReadInt32());
                    if (size != fs.Length)
                    {
                        Console.WriteLine("文件{0}大小不正确，可能已经损坏。\n检测到的大小：{1} \n实际大小：{2}", fileName, CommonHelper.FormatBytes(size), CommonHelper.FormatBytes(fs.Length));
                        return;
                    }
                    var fileCount = CommonHelper.HtoN(rdr.ReadInt32());

                    //Padding?
                    rdr.ReadInt32();

                    var headerSize = GetHeaderSize(fileCount);

                    if (size > 200 * 1048576)
                    {
                        Console.Clear();
                        Console.WriteLine("正在解包{0}，总大小{1}。\n请耐心等待，该操作可能需要几分钟。", fileName, CommonHelper.FormatBytes(size));
                    }

                    var processedSize = 0L;
                    var lastPercent = 0;
                    for (var i = 0; i < fileCount; i++)
                    {
                        var buffer = rdr.ReadBytes(0x20);
                        var filename = Encoding.ASCII.GetString(buffer).TrimEnd('\0');

                        var dir = Path.GetDirectoryName(filename);
                        if (!string.IsNullOrEmpty(dir))
                            Directory.CreateDirectory(dir);
                        else
                        {
                            Directory.CreateDirectory("root");
                            filename = "root\\" + filename;
                        }

                        var fileOffset = (uint)CommonHelper.HtoN(rdr.ReadInt32());
                        var fileSize = CommonHelper.HtoN(rdr.ReadInt32());
                        totalSize += fileSize;

                        var currentPos = fs.Position;
                        var fsOut = new FileStream(filename, FileMode.Create);
                        fs.Seek(fileOffset, SeekOrigin.Begin);
                        var bytesRemain = fileSize;
                        var bytesToRead = 0x400;
                        while (bytesRemain > 0)
                        {
                            if (bytesRemain < 0x400) bytesToRead = bytesRemain;
                            var readbytes = fs.Read(buf, 0, bytesToRead);
                            Debug.Assert(readbytes == bytesToRead);
                            fsOut.Write(buf, 0, readbytes);
                            bytesRemain -= readbytes;

                            processedSize += readbytes;
                            Console.SetCursorPosition(0, 2);
                            Console.CursorVisible = false;
                            var percent = (int)Math.Round(processedSize / (double)(size - headerSize) * 100);
                            if (percent <= lastPercent) continue;
                            lastPercent = percent;
                            Console.WriteLine("已完成{0}%...", percent);
                        }
                        Console.CursorVisible = true;
                        fsOut.Flush();
                        fsOut.Close();
                        fsOut.Dispose();
                        fs.Seek(currentPos, SeekOrigin.Begin);

                        //Padding?
                        rdr.ReadInt64();
                    }
                    Console.WriteLine("处理了{0}个文件，共{1}。", fileCount, CommonHelper.FormatBytes(totalSize));
                }
                else
                {
                    Console.WriteLine("文件格式错误。");
                }
            }
            fs.Dispose();
        }
    }
}
