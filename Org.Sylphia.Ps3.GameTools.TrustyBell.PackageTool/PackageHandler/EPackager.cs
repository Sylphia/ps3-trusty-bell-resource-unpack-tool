﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Org.Sylphia.Ps3.GameTools.TrustyBell.PackageTool.PackageHandler
{
    internal static class EPackager
    {
        private static readonly Hashtable Ht=new Hashtable();
        public static readonly Hashtable Strings=new Hashtable();
        
        public static void Unpack(string eFile)
        {
            var btxRead = false;
            var fs = new FileStream(eFile,FileMode.Open);
            using (var rdr=new BinaryReader(fs,Encoding.ASCII))
            {
                //Skip 00 00 01 81 48 XX XX XX XX XX XX XX
                fs.Seek(12, SeekOrigin.Begin);
                var fileSize = CommonHelper.NtoH(rdr.ReadUInt32());
                var codeSize = CommonHelper.NtoH(rdr.ReadUInt32());
                var dataSize = CommonHelper.NtoH(rdr.ReadUInt32());
                var dataOffset = fs.Position + codeSize;
                var otherOffset = dataOffset + dataSize;

                fs.Seek(dataOffset, SeekOrigin.Begin);
                // Begin read data sector
                while (fs.Position<otherOffset)
                {
                    var chunkId = "";
                    while (string.IsNullOrEmpty(chunkId))
                    {
                        chunkId = new string(rdr.ReadChars(4)).Trim('\0');
                    }
                    var chunkIdOffset = fs.Position - 4;

                    if(chunkId != "BTX ")
                    {
                        if (btxRead) break;

                        switch (chunkId)
                        {
                            case "CSL ":
                                // If CSL found, then move to last CSF section
                                var csfCount = CommonHelper.NtoH(rdr.ReadUInt32());
                                fs.Seek(4*(csfCount - 1), SeekOrigin.Current);
                                var lastCsfOffset = CommonHelper.NtoH(rdr.ReadUInt32());
                                fs.Seek(chunkIdOffset+lastCsfOffset, SeekOrigin.Begin);
                                break;
                            case "CSF ":
                                //Move pass CSF
                                var csfSize = CommonHelper.NtoH(rdr.ReadUInt32());
                                fs.Seek(chunkIdOffset + csfSize, SeekOrigin.Begin);
                                break;
                            default:
                                long chunkSize = CommonHelper.NtoH(rdr.ReadUInt32());
                                if(chunkSize==0 && chunkId=="Mefc")
                                {
                                    fs.Seek(0, SeekOrigin.End);
                                    Console.WriteLine("File Skipped: {0},\n {1}:{2:X}",Path.GetFileName(eFile),chunkId,chunkIdOffset);
                                    break;
                                }
                                Debug.Assert(chunkSize > 0);
                                fs.Seek(chunkSize - 8, SeekOrigin.Current);
                                break;
                        }
                    }
                    else
                    {
                        //Found btx
                        //Read Btx header
                        btxRead = true;
                        var btxOffset = fs.Position - 4;
                        var btxHeaderSize = CommonHelper.NtoH(rdr.ReadUInt32());
                        var btxBlockLength = CommonHelper.NtoH(rdr.ReadUInt32());
                        var btxLanguageCount = CommonHelper.NtoH(rdr.ReadUInt32());
                        fs.Seek(-0x10, SeekOrigin.Current);
                        fs.Seek(btxHeaderSize, SeekOrigin.Current);
                        //Read language header
                        for (var i = 0; i < btxLanguageCount; i++)
                        {
                            var lngOffset = fs.Position;
                            var lngId = new string(rdr.ReadChars(4));
                            var lngHeaderSize = CommonHelper.NtoH(rdr.ReadUInt32());
                            var lngBlockSize = CommonHelper.NtoH(rdr.ReadUInt32());
                            var codePage = CommonHelper.NtoH(rdr.ReadInt32());
                            var stringCount = CommonHelper.NtoH(rdr.ReadInt32());
                            if(stringCount==0) continue;

                            if (lngId == "JPN " && codePage == 0) codePage = 932;

                            //Read strings
                            var lastStringEndPos = 0L;
                            for (var j = 0; j < stringCount; j++)
                            {

                                var stringId = CommonHelper.NtoH(rdr.ReadInt32());
                                var stringOffset = CommonHelper.NtoH(rdr.ReadInt32());
                                var stringTablePos = fs.Position;

                                fs.Seek(lngOffset + stringOffset, SeekOrigin.Begin);

                                var str = CommonHelper.ReadLPZStr(rdr,  (int)(lngBlockSize == 0 ? 0x100 : lngBlockSize),
                                                                  Encoding.GetEncoding(codePage));
                                if(lngId=="JPN ")
                                    Strings.Add(
                                        string.Format("@ {0}:0x{1:X}:{2}:{3}", Path.GetFileName(eFile), chunkIdOffset, lngId,
                                                      stringId), str);

                                lastStringEndPos = fs.Position;
                                fs.Seek(stringTablePos, SeekOrigin.Begin);
                            }
                            //for test,skip string

                            if(lngBlockSize>0)
                            {
                                fs.Seek(lngOffset + lngBlockSize, SeekOrigin.Begin);
                            }
                            else if(stringCount>0)
                            {
                                fs.Seek(lastStringEndPos, SeekOrigin.Current);
                            }

                        }

                        if (btxBlockLength != 0)
                        {
                            fs.Seek(btxOffset, SeekOrigin.Begin);
                            fs.Seek(btxBlockLength, SeekOrigin.Current);
                        }
                    }

                    if (!Ht.ContainsKey(chunkId)) 
                        Ht.Add(chunkId, Path.GetFileName(eFile) + " : " + chunkIdOffset.ToString("X"));
                }
            }
        }
    }
}
