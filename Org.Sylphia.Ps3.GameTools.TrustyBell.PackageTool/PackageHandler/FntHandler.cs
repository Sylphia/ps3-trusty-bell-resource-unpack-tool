﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace Org.Sylphia.Ps3.GameTools.TrustyBell.PackageTool.PackageHandler
{
    internal static class FntHandler
    {
        private const int FntFileMagic = 0x464f4e54;
        public static readonly Hashtable GlyphData=new Hashtable();
        public static void Parse(string fntFile)
        {
            var fs = File.OpenRead(fntFile);
            using (var rdr=new BinaryReader(fs))
            {
                var magic = CommonHelper.NtoH( rdr.ReadInt32());
                if(magic!=FntFileMagic) return;

                var fileSize = CommonHelper.NtoH(rdr.ReadUInt32());
                //3 unknown int
                rdr.ReadInt32();
                rdr.ReadInt32();
                rdr.ReadInt32();

                //Skip 1st sector, assume it as single byte glyphs
                var bcPos = fs.Position;
                var bcMagic = CommonHelper.NtoH(rdr.ReadInt32());  //BC  (Byte Characters)
                var bcSize = CommonHelper.NtoH(rdr.ReadUInt32());
                fs.Seek(bcPos + bcSize, SeekOrigin.Begin);

                var mbcPos = fs.Position;
                var mbcMagic = CommonHelper.NtoH(rdr.ReadInt32());  //MBC (MultiByte Characters)
                var mbcSize = CommonHelper.NtoH(rdr.ReadUInt32());
                var mbcCount = CommonHelper.NtoH(rdr.ReadInt32());

                var mbcTablePos = fs.Position; 
                var mbcTableMagic = CommonHelper.NtoH(rdr.ReadInt32()); //TABL
                var mbcTableSize = CommonHelper.NtoH(rdr.ReadUInt32());
                var chars = new string[mbcCount];

                for (var i = 0; i < mbcCount; i++)
                {
                    var buffer=rdr.ReadBytes(2);
                    chars[i] = Encoding.GetEncoding(932).GetString(buffer);
                }

                // Read glyph data
                fs.Seek(mbcTablePos + mbcTableSize, SeekOrigin.Begin);

                var dataOffsets = new uint[mbcCount];
                var mbcDataPos = mbcTablePos + mbcTableSize;
                var mbcDataMagic = CommonHelper.NtoH(rdr.ReadInt32()); //DATA
                var mbcDataSize = CommonHelper.NtoH(rdr.ReadUInt32());

                for (var i = 0; i < mbcCount; i++)
                {
                    dataOffsets[i] = CommonHelper.NtoH(rdr.ReadUInt32());
                }

                for (var i = 0; i < mbcCount; i++)
                {
                    var dataLen = i == mbcCount - 1 ? mbcDataSize - dataOffsets[i] : dataOffsets[i + 1] - dataOffsets[i];
                    fs.Seek(mbcDataPos + dataOffsets[i], SeekOrigin.Begin);
                    var pos = new Point(rdr.ReadByte(), rdr.ReadByte());
                    var wh = new Point((byte)CommonHelper.PaddingInt( rdr.ReadByte(),4), rdr.ReadByte());
                    var desc = new Rectangle(pos, new Size(wh));
                    var data=rdr.ReadBytes((int)dataLen-4);
                    GlyphData.Add(chars[i],new KeyValuePair<Rectangle,byte[]>(desc,data));
                }
            }
        }
    }
}
