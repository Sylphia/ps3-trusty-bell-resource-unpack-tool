﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Org.Sylphia.Ps3.GameTools.TrustyBell.PackageTool.PackageHandler
{
    internal static class ImageHelper
    {
        public static Bitmap DrawFont(int x, int y, int width, int height, byte[] data)
        {
            var image = new Bitmap(x + width, y + height);

            for (var i = 0; i < height; i++)
            {
                for (var j = 0; j < (width/4); j++)
                {
                    for (var k = 0; k < 4; k++)
                    {
                        var red = ((data[((i*width)/4) + j] >> ((3 - k)*2)) & 3)*0x55;
                        image.SetPixel(((j*4) + k) + x, i + y, Color.FromArgb(red, red, red));
                    }
                }
            }
            return image;
        }

        public static Bitmap ReSize(Bitmap bm, int pow)
        {
            var image = new Bitmap(bm.Width*pow, bm.Height*pow);
            var graphics = Graphics.FromImage(image);
            graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
            graphics.DrawImage(bm, 0, 0, image.Width, image.Height);
            graphics.Save();
            graphics.Dispose();
            return image;
        }
    }
}