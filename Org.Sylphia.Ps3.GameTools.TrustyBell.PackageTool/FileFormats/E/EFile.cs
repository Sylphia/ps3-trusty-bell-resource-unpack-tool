﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Org.Sylphia.Ps3.GameTools.TrustyBell.PackageTool.FileFormats.E
{
    public class EFile
    {
        private byte[] _otherDataBlock;
        #region 属性

        public EFileHeader Header { get; set; }
        public uint DataOffset { get; set; }
        
        public uint OtherDataOffset { get; set; }

        public long BTXChunkOffset { get; set; }

        private byte[] _dataAfterBtx;
        private BTXChunk BtxData { get; set; }
        #endregion

        #region 构造、析构

        public EFile(string fileName)
        {
            Read(fileName);
        }

        #endregion

        #region 公有方法

        #endregion

        #region 私有方法

        private void Read(string fileName)
        {
            var btxRead = false;
            var fs = new FileStream(fileName, FileMode.Open);
            using (var rdr = new BinaryReader(fs, Encoding.ASCII))
            {
                Header=new EFileHeader(rdr);
                DataOffset = EFileHeader.HeaderSize + Header.CodeChunkSize;
                OtherDataOffset = DataOffset + Header.DataChunkSize;

                fs.Seek(DataOffset, SeekOrigin.Begin);

                while (fs.Position < OtherDataOffset)
                {
                    var chunkId = "";
                    while (string.IsNullOrEmpty(chunkId))
                    {
                        chunkId = new string(rdr.ReadChars(4)).Trim('\0');
                    }
                    var chunkIdOffset = fs.Position - 4;

                    if(!btxRead)
                        if (chunkId != "BTX ")
                        {
                            if (btxRead) break;

                            switch (chunkId)
                            {
                                case "CSL ":
                                    // If CSL found, then move to last CSF section
                                    var csfCount = CommonHelper.NtoH(rdr.ReadUInt32());
                                    fs.Seek(4 * (csfCount - 1), SeekOrigin.Current);
                                    var lastCsfOffset = CommonHelper.NtoH(rdr.ReadUInt32());
                                    fs.Seek(chunkIdOffset + lastCsfOffset, SeekOrigin.Begin);
                                    break;
                                case "CSF ":
                                    //Move pass CSF
                                    var csfSize = CommonHelper.NtoH(rdr.ReadUInt32());
                                    fs.Seek(chunkIdOffset + csfSize, SeekOrigin.Begin);
                                    break;
                                default:
                                    long chunkSize = CommonHelper.NtoH(rdr.ReadUInt32());
                                    if (chunkSize == 0 && chunkId == "Mefc")
                                    {
                                        fs.Seek(0, SeekOrigin.End);
                                        Console.WriteLine("File Skipped: {0},\n {1}:{2:X}", Path.GetFileName(fileName), chunkId, chunkIdOffset);
                                        break;
                                    }
                                    Debug.Assert(chunkSize > 0);
                                    fs.Seek(chunkSize - 8, SeekOrigin.Current);
                                    break;
                            }
                        }
                        else
                        {
                            //Found btx
                            //Read Btx header
                            btxRead = true;
                            BTXChunkOffset = fs.Position - 4;
                            fs.Seek(-4, SeekOrigin.Current);
                            BtxData=new BTXChunk(rdr);

                            if(OtherDataOffset!=fs.Position)
                            {
                                _dataAfterBtx = rdr.ReadBytes((int)(OtherDataOffset - fs.Position));
                            }
                        }
                }
                _otherDataBlock = rdr.ReadBytes((int)(Header.FileSize - OtherDataOffset));
            }
            fs.Close();
            fs.Dispose();
        }

        #endregion

         
    }
}