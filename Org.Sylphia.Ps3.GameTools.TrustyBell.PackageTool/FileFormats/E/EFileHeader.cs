﻿using System;
using System.IO;

namespace Org.Sylphia.Ps3.GameTools.TrustyBell.PackageTool.FileFormats.E
{
    public struct EFileHeader
    {

        #region 属性

        public const int HeaderSize = 24;

        public int Magic { get; set; }
        public long HeaderUnknown { get; set; }
        public uint FileSize { get; set; }
        public uint CodeChunkSize { get; set; }
        public uint DataChunkSize { get; set; }

        #endregion

        public EFileHeader(BinaryReader rdr):this()
        {
            Read(rdr);
        }

        private void Read(BinaryReader rdr)
        {
            rdr.BaseStream.Seek(0, SeekOrigin.Begin);

            Magic = rdr.ReadInt32();
            HeaderUnknown = rdr.ReadInt64();
            FileSize = CommonHelper.NtoH(rdr.ReadUInt32());
            CodeChunkSize = CommonHelper.NtoH(rdr.ReadUInt32());
            DataChunkSize = CommonHelper.NtoH(rdr.ReadUInt32());
        }
        #region 公有方法

        public void Write(BinaryWriter wtr)
        {
            wtr.BaseStream.Seek(0, SeekOrigin.Begin);

            wtr.Write(Magic);
            wtr.Write(HeaderUnknown);
            wtr.Write(CommonHelper.HtoN(FileSize));
            wtr.Write(CommonHelper.HtoN(CodeChunkSize));
            wtr.Write(CommonHelper.HtoN(DataChunkSize));
        }

        #endregion
    }
}