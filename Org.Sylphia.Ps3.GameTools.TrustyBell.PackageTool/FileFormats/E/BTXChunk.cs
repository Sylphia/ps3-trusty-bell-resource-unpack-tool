﻿using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

namespace Org.Sylphia.Ps3.GameTools.TrustyBell.PackageTool.FileFormats.E
{
    public struct BTXChunk
    {
        public const string ChunkId = "BTX ";
        public const int BtxHeaderSize = 0x10;
        public uint ChunkSize { get; set; }
        public uint LocaleCount { get; set; }

        private IList<LocaleBlock> Locales { get; set; } 

        public BTXChunk(BinaryReader rdr):this()
        {
            Read(rdr);    
        }

        private void Read(BinaryReader rdr)
        {
            rdr.ReadInt32();
            rdr.ReadInt32();
            ChunkSize = CommonHelper.NtoH(rdr.ReadUInt32());
            LocaleCount = CommonHelper.NtoH(rdr.ReadUInt32());
            Locales = new List<LocaleBlock>();
            for (int i = 0; i < LocaleCount; i++)
            {
                Locales.Add(new LocaleBlock(rdr));
            }
        }

        public void ReplaceString(string localeId,int id,string newString)
        {
            var locale = Locales.First(x => x.LocaleId == localeId);
            locale.ReplaceString(id,newString);
            ChunkSize = (uint)(BtxHeaderSize + Locales.Sum(x => x.LocaleBlockSize));
        }

        public void Write(BinaryWriter wtr)
        {
            wtr.Write(Encoding.ASCII.GetBytes(ChunkId));
            wtr.Write(CommonHelper.HtoN(BtxHeaderSize));
            wtr.Write(CommonHelper.HtoN(ChunkSize));
            wtr.Write(CommonHelper.HtoN(LocaleCount));
            foreach (var localeBlock in Locales)
            {
                localeBlock.Write(wtr);
            }
        }
    }

    public struct LocaleBlock
    {
        public string LocaleId { get; set; }
        public const uint LocaleHeaderSize = 0x14;
        public uint LocaleBlockSize { get; set; }
        public int CodePage { get; set; }
        public uint StringCount { get; set; }

        private Encoding _encoding;
        
        public LocaleBlock(BinaryReader rdr):this()
        {
            Read(rdr);
        }

        public IList<KeyValuePair<int, int>> StringTable { get; private set; }

        public IList<string> Strings { get; set; } 

        public void ReplaceString(int id,string newString)
        {
            var index = StringTable.IndexOf(StringTable.First(x => x.Key == id));
            var orginalByteLength = _encoding.GetBytes(Strings[index]).Length;
            
            var newByteLength=_encoding.GetBytes(newString).Length;
            //若新字串长度较短，则以\0填充至原长度
            var str = newByteLength < orginalByteLength
                          ? newString + string.Empty.PadRight(orginalByteLength - newByteLength, '\0')
                          : newString;

            Strings[index] = str;

            var offset = newByteLength-orginalByteLength;
            //更新字符串偏移量
            if (newByteLength > orginalByteLength)
            {
                for (int i = index + 1; i < StringTable.Count; i++)
                {
                    StringTable[i] = new KeyValuePair<int, int>(StringTable[i].Key, StringTable[i].Value + offset);
                }
                LocaleBlockSize = (uint)(LocaleBlockSize+offset);
            }
        }

        private void Read(BinaryReader rdr)
        {
            var blockOffset = rdr.BaseStream.Position;

            LocaleId = new string(rdr.ReadChars(4));
            rdr.ReadInt32();
            LocaleBlockSize = CommonHelper.NtoH(rdr.ReadUInt32());
            CodePage = CommonHelper.NtoH(rdr.ReadInt32());
            _encoding = Encoding.GetEncoding(CodePage);
            StringCount = CommonHelper.NtoH(rdr.ReadUInt32());
            StringTable=new List<KeyValuePair<int, int>>(); //string id,offset
            Strings=new List<string>();

            for (var i = 0; i < StringCount; i++)
            {
                StringTable.Add(
                    new KeyValuePair<int, int>(CommonHelper.NtoH(rdr.ReadInt32()), CommonHelper.NtoH(rdr.ReadInt32())));
            }

            foreach (var pair in StringTable)
            {
                rdr.BaseStream.Seek(blockOffset + pair.Value, SeekOrigin.Begin);
                Strings.Add(CommonHelper.ReadLPZStr(rdr, 0x1000, _encoding));
            }
        }

        public void Write(BinaryWriter wtr)
        {
            var blockOffset = wtr.BaseStream.Position;

            wtr.Write(Encoding.ASCII.GetBytes(LocaleId));
            wtr.Write(CommonHelper.HtoN(LocaleHeaderSize));
            wtr.Write(CommonHelper.HtoN(LocaleBlockSize));
            wtr.Write(CommonHelper.HtoN(CodePage));
            wtr.Write(CommonHelper.HtoN(StringCount));

            for (int i = 0; i < StringCount; i++)
            {
                wtr.Write(CommonHelper.HtoN(StringTable[i].Key));
                wtr.Write(CommonHelper.HtoN(StringTable[i].Value));
            }
            for (int i = 0; i < StringCount; i++)
            {
                wtr.BaseStream.Seek(blockOffset + StringTable[i].Value, SeekOrigin.Begin);
                wtr.Write(_encoding.GetBytes(Strings[i]));
                wtr.Write((byte)0);
            }

        }
    }
}